# Ideas

## Crónicas

### Sobre las salteñas

Una historia de las salteñas, las discusiones y las formas de comerlas. ¿Por qué solo de día? Técnicas para comerlas sin cuchara y de parado. Cómo hacerlas (podría incluirse en medio la receta y me intento de llevarla a la práctica).

Salteñas de hoja, es una masa de hojaldre. Siguen siendo más interesantes las comunes.

Nacen en Potosí, supuestamente de una española que tenía que esperar a su marido minero, encerrada por el frío y las inclemencias de Potosí, junto a sus criadas decidieron buscar una receta que mantuviera el calor hasta que llegara el esposo. Así encontraron la receta con la gelatina que mantiene el caldo caliente. La gelatina es también la razón por la cual la masa no se humedece, porque el relleno ya frio está en estado gelatinoso.

### Teleféricos

La historia de la creación de los teleféricos, un paseo por cada una de las líneas y qué se ve de cada una de ellas (por ejemplo la montaña de la verde que separa los ricos de los pobres, el auto clavado en la montaña). Imágenes de la ciudad.